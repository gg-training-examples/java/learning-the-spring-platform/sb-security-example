package com.groundgurus.sbsecurityexample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class SimpleController {
  @GetMapping("/")
  String index(Model model, Principal principal) {
    model.addAttribute("name", principal.getName());
    return "user/index";
  }

  @GetMapping("/admin")
  String adminPage(Model model, Principal principal) {
    model.addAttribute("name", principal.getName());
    return "admin/admin";
  }
}
