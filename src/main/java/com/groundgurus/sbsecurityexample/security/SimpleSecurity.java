package com.groundgurus.sbsecurityexample.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.List;

@EnableWebSecurity
public class SimpleSecurity extends WebSecurityConfigurerAdapter {
  private PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

  @Bean
  UserDetailsService authentication() {
    UserDetails user1 = User.builder()
      .username("duke")
      .password(passwordEncoder.encode("password"))
      .roles("USER")
      .build();

    UserDetails user2 = User.builder()
      .username("jessy")
      .password(passwordEncoder.encode("password"))
      .roles("USER", "ADMIN")
      .build();

    return new InMemoryUserDetailsManager(user1, user2);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .authorizeRequests()
      .mvcMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
      .and()
      .authorizeRequests()
      .anyRequest().authenticated()
      .and()
      .formLogin()
      .and()
      .httpBasic();
  }
}
